package regexp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpGrouping {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("Java (?!7|8)");
        Matcher matcher1 = pattern.matcher("Java 7 Java 7");
        Matcher matcher2 = pattern.matcher("Java 9");

        if(!matcher1.find()) System.out.println("Coincidence not found");

        while (matcher2.find()) {
            System.out.println(matcher2.group());
        }
    }
}
