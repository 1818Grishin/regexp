package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReluctantQuantification {
    public static void main(String[] args) {
        int counter = 0;
        String string = "196.198.1.197";
        Pattern pattern = Pattern.compile(".*?19");
        Matcher matcher = pattern.matcher(string);

        while(matcher.find()){
            counter++;
            System.out.println("Match found '" +
                    string.substring(matcher.start(), matcher.end()) +
                    "'. Starting at index " + matcher.start() +
                    " and ending at index " + matcher.end());
        }

        System.out.println("Matches found: " + counter);
    }
}
